<?php
$CONFIG = array (
  'htaccess.RewriteBase' => '/',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/var/www/html/apps',
      'url' => '/apps',
      'writable' => false,
    ),
    1 => 
    array (
      'path' => '/var/www/html/custom_apps',
      'url' => '/custom_apps',
      'writable' => true,
    ),
  ),
  'instanceid' => 'ocnvezyvi76a',
  'passwordsalt' => '1snyMo+UuIl6/xjWxkBFl/hnKg59P8',
  'secret' => 'lIKZ3D+UIh6aJfijpKJB6bRNgX1a+Ybx+L0dzorf8UOk6ZTi',
  'trusted_domains' => 
    array (
      0 => 'localhost',
      1 => 'your_local_ip_address'
    ),
  'datadirectory' => '/var/www/html/data',
  'dbtype' => 'sqlite3',
  'version' => '20.0.0.9',
  'overwrite.cli.url' => 'http://localhost',
  'config_is_read_only' => true,
  'installed' => true,
);
